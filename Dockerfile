# Copyright (c) 2023 Hiroyuki Okada
# This software is released under the MIT License.
# http://opensource.org/licenses/mit-license.php
#FROM nvidia/opengl:base-ubuntu22.04 
#FROM nvidia/opengl:1.0-glvnd-devel-ubuntu22.04
FROM nvcr.io/nvidia/cuda:11.7.1-cudnn8-devel-ubuntu22.04

LABEL maintainer="Hiroyuki Okada <hiroyuki.okada@okadanet.org>"
LABEL org.okadanet.vendor="Hiroyuki Okada" \
      org.okadanet.dept="TRCP" \
      org.okadanet.version="1.0.0" \
      org.okadanet.released="July 11, 2023"

SHELL ["/bin/bash", "-c"]
ARG DEBIAN_FRONTEND=noninteractive

# nvidia-container-runtime
ENV NVIDIA_VISIBLE_DEVICES \
    ${NVIDIA_VISIBLE_DEVICES:-all}
ENV NVIDIA_DRIVER_CAPABILITIES \
    ${NVIDIA_DRIVER_CAPABILITIES:+$NVIDIA_DRIVER_CAPABILITIES,}graphics
ENV __NV_PRIME_RENDER_OFFLOAD=1
ENV __GLX_VENDOR_LIBRARY_NAME=nvidia
ARG ROS_DISTRO=humble

# Timezone, Launguage設定
RUN apt update \
  && apt install -y --no-install-recommends \
     locales \
     software-properties-common tzdata \
  && locale-gen ja_JP ja_JP.UTF-8  \
  && update-locale LC_ALL=ja_JP.UTF-8 LANG=ja_JP.UTF-8 \
  && add-apt-repository universe

# Locale
ENV LANG ja_JP.UTF-8
ENV TZ=Asia/Tokyo

# install packages
RUN apt-get update && apt-get install -q -y --no-install-recommends \
    curl sudo \
    gnupg \
    lsb-release git \
  && rm -rf /var/lib/apt/lists/*


# install ROS2 humble
ENV UBUNTU_CODENAME=focal
RUN curl -s https://raw.githubusercontent.com/ros/rosdistro/master/ros.asc | apt-key add - \
    && echo "deb http://packages.ros.org/ros2/ubuntu $(lsb_release -cs) main" > /etc/apt/sources.list.d/ros2-latest.list
RUN apt-get update && apt-get install -y \
    ros-humble-desktop  \ 
    python3-colcon-common-extensions \
    ros-humble-rqt-* \
    && rm -rf /var/lib/apt/lists/*
RUN sudo apt-get update && sudo apt-get install -q -y --no-install-recommends \
  git wget \
  x11-utils x11-apps terminator xterm xauth \
  terminator xterm nano vim htop \
  python3-rosdep  \
  ros-humble-v4l2-camera \
  && sudo rm -rf /var/lib/apt/lists/*
RUN sudo rosdep init && rosdep update


# Add user and group
ARG UID
ARG GID
ARG USER_NAME
ARG GROUP_NAME
ARG PASSWORD

RUN groupadd -g $GID $GROUP_NAME && \
    useradd -m -s /bin/bash -u $UID -g $GID -G sudo $USER_NAME && \
    echo $USER_NAME:$PASSWORD | chpasswd && \
    echo "$USER_NAME   ALL=(ALL) NOPASSWD:ALL" >> /etc/sudoers
USER ${USER_NAME}

RUN cd ~/ && mkdir -p ~/ros2_ws/src && \
 cd ros2_ws/src && git clone --recursive  https://github.com/Ar-Ray-code/darknet_ros_yolov4.git && \
 darknet_ros_yolov4/darknet_ros/rm_darknet_CMakeLists.sh && \
 cd ~/ros2_ws && /bin/bash -c "source /opt/ros/humble/setup.sh; colcon build"

# Config (if you wish)
RUN mkdir -p ~/.config/terminator/
COPY assets/terminator_config /home/$USER_NAME/.config/terminator/config 
COPY assets/entrypoint.sh /entrypoint.sh
RUN sudo chmod a+x /entrypoint.sh

RUN echo "source /opt/ros/humble/setup.bash" >> ~/.bashrc
RUN echo "source ~/ros2_ws/install/setup.bash" >> ~/.bashrc

CMD ["terminator"]
